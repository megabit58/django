from django.db import models
from django.contrib.auth.models import User

class Application(models.Model):
    """
    модель заявки на проход посещения
    """
    status_choices = ( # статус заявки ["на рассмотрении", "принята", "отклонена"]
        ('AC', 'accepted'),
        ('RJ', 'rejected'),
        ('PR', 'processing'),
    )

    target = models.CharField(max_length=200) # цель визита

    visitor_id = models.ForeignKey(User, related_name = 'visitor_id', on_delete=models.SET_NULL, null=True, blank=True) # автор заявки
    time = models.DateTimeField(auto_now_add=True) # время отправки заявки    
    status = models.CharField(max_length=200, choices=status_choices, default='PR') # статус заявки
    manager_id = models.ForeignKey(User, related_name = 'manager_id', on_delete=models.SET_NULL, null=True, blank=True) # менеджер, рассмотревший заявку    

    class Meta:
        verbose_name = "Application"
        verbose_name_plural = "Application"
    
    def __str__(self):
        return str(self.id)
   

