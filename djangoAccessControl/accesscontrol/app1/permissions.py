from django.contrib.auth.models import Group
from rest_framework import permissions

def is_in_group(user: 'User', group_name: str) -> bool:
  """
  Принимает пользователя и имя группы и возвращает True, если пользователь находится в этой группе
  """
  group = Group.objects.filter(name=group_name).first()
  if group:
    return group.user_set.filter(id=user.id).exists()
  return False

class HasGroupPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        # отображение методов -> требуемая группа.
        required_groups_mapping = getattr(view, "required_groups", {})

        # Определение необходимых групп для этого метода запроса
        required_groups = required_groups_mapping.get(request.method, [])

        # True, если пользователь в группе или  является staff
        
        for group_name in required_groups:
            if group_name == "__all__":
                return True
            if group_name == "__none__":
                return False
            return is_in_group(request.user, group_name) or request.user.is_staff
