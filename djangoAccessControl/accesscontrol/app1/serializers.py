from rest_framework import serializers
from .models import Application

class ApplicationPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Application
        fields = ['target', 'time']

class ApplicationGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Application
        fields = '__all__'

class UpdateApplicationPostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Application
        fields = ['status']

