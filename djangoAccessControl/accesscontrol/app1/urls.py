from django.urls import path, include
from rest_framework import routers
from .views import NewApplicationsList, ApplicationsList, ApplicationCreate, MyApplicationsList, ApplicationUpdate

urlpatterns = [
    path('requests/', ApplicationsList.as_view(), name='ApplicationsList'),
    path('requests/new/', ApplicationCreate.as_view(), name='ApplicationCreate'),
    path('requests/my/', MyApplicationsList.as_view(), name='MyApplicationViewSet'),
    path('requests/news/', NewApplicationsList.as_view(), name='NewApplicationsList'),
    path('requests/<int:pk>/status/', ApplicationUpdate.as_view(), name='AccApplicationUpdate'),
    path('requests/<int:pk>/status/', ApplicationUpdate.as_view(), name='RejApplicationUpdate'),
]

