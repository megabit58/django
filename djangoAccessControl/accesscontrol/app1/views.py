#from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets, generics
from rest_framework.decorators import detail_route, api_view
from rest_framework import serializers 
from .models import Application
from .serializers import ApplicationPostSerializer, ApplicationGetSerializer, UpdateApplicationPostSerializer
from .permissions import HasGroupPermission
from rest_framework.permissions import IsAuthenticated


class ApplicationsList(generics.ListAPIView):
    """
    Список всех заявок
    """
    queryset = Application.objects.all()
    serializer_class = ApplicationGetSerializer
    permission_classes = [HasGroupPermission]
    required_groups = {
         'GET': ['managers'],
    }
    
class ApplicationCreate(generics.CreateAPIView):
    """
    Отправка заявки
    """
    queryset = Application.objects.all()
    serializer_class = ApplicationPostSerializer
    permission_classes = [HasGroupPermission]
    required_groups = {
         'POST': ['visitors'],
    }
    def perform_create(self, serializer):
        serializer.save(visitor_id=self.request.user) # добавляем автора заявки
        
class NewApplicationsList(ApplicationsList):
    """
    Список необработанных заявок
    """
    def get_queryset(self): # фильтр по статусу заявки
        return Application.objects.filter(status='PR')
        
class MyApplicationsList(ApplicationsList):
    """
    Список моих (отправленных) заявок
    """
    permission_classes = [HasGroupPermission]
    required_groups = {
         'GET': ['visitors'],
    }
    def get_queryset(self): # фильтр по статусу заявки
        return Application.objects.filter(visitor_id=self.request.user)

class ApplicationUpdate(generics.UpdateAPIView):
    """
    Рассмотреть заявку
    'AC' -- принять, 'RJ' -- отклонить
    """
    queryset = Application.objects.all()
    serializer_class = UpdateApplicationPostSerializer
    permission_classes = [HasGroupPermission]
    required_groups = {
         'PUT': ['__none__'],
         'PATCH': ['managers'],
    }
    def perform_update(self, serializer):
        serializer.save(manager_id=self.request.user) # добавляем менеджера, рассмотревшего заявку
